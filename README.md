# LBM CERNN

LBM (Lattice Boltzmann method) project for flow simulation, using C/C++ and CUDA API. The velocity sets supported are D3Q19 and D3Q27.

## Features

The features supported by LBM are:

* Boundary Conditions
    * Zou-He (velocity or pressure)
    * Bounce-back (velocity)
    * Free slip

* Collision
    * BGK operator
    * Regularization

* Velocity Sets
    * D3Q19
    * D3Q27

* Constant force

Other features can be implemented or changed. The file [updating.md](./doc/updating.md) gives more details on that.

## Compilation

The requirements are:
* Nvidia drivers must be installed
* CUDA API must be installed

Both can be obtained in "CUDA Toolkit", provided by Nvidia.

The code supports Nvidia's GPUs with compute capability 3.5 or higher. The program runs in only one GPU, multi-GPU support is in development.

For compilation, a [bash file](./src/CUDA/compile.sh) is provided. It contains the commands used to compile and the instructions for altering it according to the GPU compute capability and the arguments to pass to it.

## Usage

Code usage is described in [updating.md](./doc/updating.md). The file describes how to change simulation parameters, boundary conditions, collision scheme, etc. 

## Documentation

The documentation of the source files is made in markDown files ([files.md](./doc/files.md) and [updating.md](./doc/updating.md)) and in header files (_.h_), with each function presenting _@brief_, _@param_ and _@return_, describing the function, its parameters and what it returns.

## Post Processing

Since the program exports macroscopics in binary format, it is necessary to process it. For that, Python source files are provided. The package dependecies are:
* glob
* numpy
* os
* pyevtk
* matplotlib

Two example files are presented in _CUDA/Post Processing/_ folder. Basic data treatment, exportation and plot is implemented by these files. Graphics f(x)=y, heatmaps and exportation to VTK and .csv are the examples provided. Feel free to alter these to match the required data processing.

Implementations details and more informations can be obtained in the source files and in [files.md](./doc/files.md).

## License

This software is provided under the [GPLv2 license](./LICENSE.txt).

## Contact

For bug report or issue adressing, use of git resource is encouraged. Contact via email is: _waine@alunos.utfpr.edu.br_ and/or _cernn-ct@utfpr.edu.br_.
