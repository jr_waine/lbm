100 - 103: initial tests
104 - 105: collision and streaming along
106 - 107: collision and equilibrium generic
108 - 109: macroscopics generic
110 - 111: population 0 and 1-8 together
112 - 113: streaming generic
114 - 115: population 0 and 1-8 together and collision and streaming along
116 - 117: boundary conditions generic
118 - 119: macroscopics, collision and streaming along