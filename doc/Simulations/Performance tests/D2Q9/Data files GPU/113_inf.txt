SIMULATION INFORMATION:
       Program's ID: 1.0
                 NX: 1024
                 NY: 1024
           Reynolds: 10.00
                Tau: 1.586000e+01
               Umax: 5.000000e-02
           Residual: 1.000000e+00
             Nsteps: 5000
              MLUPS: 1122.8
          Bandwidht: 150.6 (Gb/s)

CUDA INFORMATION
               name: Tesla K20Xm
    multiprocessors: 14
 compute capability: 3.5
        ECC enabled: 0
