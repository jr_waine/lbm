SIMULATION INFORMATION:
       Program's ID: 1.0
                 NX: 128
                 NY: 128
           Reynolds: 10.00
                Tau: 2.420000e+00
               Umax: 5.000000e-02
           Residual: 1.345418e-06
             Nsteps: 32000
              MLUPS: 673.0
          Bandwidht: 90.3 (Gb/s)

CUDA INFORMATION
               name: Tesla K20Xm
    multiprocessors: 14
 compute capability: 3.5
        ECC enabled: 0
