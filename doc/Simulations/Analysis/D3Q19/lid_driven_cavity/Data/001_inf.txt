SIMULATION INFORMATION:
                 NX: 128
                 NY: 128
                 NZ: 128
           Reynolds: 10.00
                Tau: 7.400000e-01
               Umax: 6.250000e-03
           Residual: 1.000000e+00
             Nsteps: 38400
              MLUPS: 587.3
          Bandwidht: 166.3 (Gb/s)

CUDA INFORMATION
               name: Tesla K20Xm
    multiprocessors: 14
 compute capability: 3.5
        ECC enabled: 0
