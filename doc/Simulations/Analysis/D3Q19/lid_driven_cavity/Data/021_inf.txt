SIMULATION INFORMATION:
                 NX: 64
                 NY: 64
                 NZ: 64
           Reynolds: 1000.00
                Tau: 5.384000e-01
               Umax: 2.000000e-01
           Residual: 1.000000e+00
             Nsteps: 12000
              MLUPS: 598.9
          Bandwidht: 169.6 (Gb/s)

CUDA INFORMATION
               name: Tesla K20Xm
    multiprocessors: 14
 compute capability: 3.5
        ECC enabled: 0
