# TODO

* Add compilation support to D3Q27 (high)

* Validate D3Q27 (high)

* Check compability between code and documentation (high)

* Update documentation with a state diagram for the main function (low)

* Make a Makefile (high)

* Multi-GPU support (low)

* Add "mkdir" with ID_SIM for simulation saving (medium)